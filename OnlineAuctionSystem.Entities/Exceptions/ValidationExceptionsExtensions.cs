﻿using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace OnlineAuctionSystem.Entities.Exceptions
{
    public static class ValidationExceptionExtensions
    {
        public static Dictionary<string, object> GetDictionaryByValidationFailure(this ValidationFailure failure)
        {
            if (failure == null)
            {
                throw new ArgumentNullException(nameof(failure));
            }

            var returnDict = new Dictionary<string, object>
            {
                {"property", failure.PropertyName},
                {"reason", failure.ErrorMessage},
                {"error", failure.ErrorCode}
            };

            if (failure.CustomState != null)
            {
                returnDict.Add("context", failure.CustomState);
            }

            return returnDict;

        }
    }
}