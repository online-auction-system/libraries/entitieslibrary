﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;

namespace OnlineAuctionSystem.Entities.Exceptions
{
    public class JsonValidationException : FinalException
    {
        private readonly List<Dictionary<string, object>> _errorsList;
        public JsonValidationException(IEnumerable<ValidationFailure> errorList, string reason)
        {
            _errorsList = new List<Dictionary<string, object>>();
            _errorsList.AddRange(errorList.Select(error => error.GetDictionaryByValidationFailure()));

            HttpErrorCode = 400;
            Reason = reason;
        }
        
        public JsonValidationException(IEnumerable<ValidationFailure> errorList)
        {
            _errorsList = new List<Dictionary<string, object>>();
            _errorsList.AddRange(errorList.Select(error => error.GetDictionaryByValidationFailure()));

            HttpErrorCode = 400;
            Reason = "Validation exception";
        }

        public override JsonResult ToJson()
        {
            return new JsonResult(
                    new
                    {
                        Reason,
                        ErrorsList = _errorsList
                    }
                )
                { StatusCode = HttpErrorCode };
        }
    }
}