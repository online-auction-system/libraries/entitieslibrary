﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineAuctionSystem.Entities.Exceptions
{
    public class DeserializationException : FinalException
    {
        private const string ErrorReason = "desirialization_error";
        public string ErrorFields { get; set; }

        public DeserializationException(string errorFields, int httpErrorCode, string reason) 
            : base(httpErrorCode, reason)
        {
            ErrorFields = errorFields;
        }
        
        public DeserializationException(string errorFields) 
            : base(400, ErrorReason)
        {
            ErrorFields = errorFields;
        }
        
        public override JsonResult ToJson()
        {
            return new JsonResult(new
            {
                Reason, ErrorFields
            })
            {
                StatusCode = HttpErrorCode
            };
        }
    }
}