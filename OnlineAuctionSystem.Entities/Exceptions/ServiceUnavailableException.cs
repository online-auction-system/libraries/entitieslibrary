﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineAuctionSystem.Entities.Exceptions
{
    public class ServiceUnavaliableException : FinalException
    {
        public Services Service { get; }

        public ServiceUnavaliableException(Services service)
        {
            HttpErrorCode = 503;
            Service = service;
            Reason = "Сервис недоступен";
        }

        public override string ToString()
        {
            return string.Format($"Сервис {Service.ToString()} недоступен");
        }

        public override JsonResult ToJson()
        {
            return new JsonResult(new
            {
                Reason, Service
            })
            {
                StatusCode = HttpErrorCode
            };
        }
    }
}