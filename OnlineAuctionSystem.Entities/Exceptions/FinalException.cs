﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace OnlineAuctionSystem.Entities.Exceptions
{
    public class FinalException: Exception
    {
        public int HttpErrorCode { get; set; }
        public string Reason { get; set; }

        public FinalException()
        {
            HttpErrorCode = 500;
            Reason = "internal_service_error";
        }

        public FinalException(int httpErrorCode, string reason)
        {
            HttpErrorCode = httpErrorCode;
            Reason = reason;
        }

        public virtual JsonResult ToJson()
        {
            return new JsonResult(new
            {
                Reason
            })
            {
                StatusCode = HttpErrorCode
            };
        }
    }
}